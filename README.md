# AWS CDK S3 Bucket Project

This project demonstrates how to create an Amazon S3 bucket with versioning and server-side encryption enabled using the AWS Cloud Development Kit (CDK) in TypeScript.

## Overview

The AWS CDK application in this project defines an infrastructure that includes a single S3 bucket. The bucket is configured to have versioning enabled and uses S3-managed encryption (SSE-S3) for data at rest.

## Features

This project sets up the following AWS resources:

- **Amazon S3 Bucket**: An S3 bucket with the following features:
  - **Versioning**: Enabled to keep multiple versions of an object in the same bucket.
  - **Encryption**: Uses Amazon S3-managed keys (SSE-S3) for server-side encryption.

## Structure
- `lib/`: This directory contains the main stack definition (`my_cdk_project-stack.ts`), where the S3 bucket is defined.
- `bin/`: Contains the entry point (`my_cdk_project.ts`) for the CDK application.
- `node_modules/`: Contains project dependencies.
- `cdk.json`: CDK configuration file.

## CodeWhisper Usage
Install the AWS Toolkit for Visual Studio Code. Create AWS Builder ID to enable Code Whisper.
![CDK Code Snippet](9d11c995ee0545b852608d8df4660b4.png "CDK Code Snippet")

I add the commit "//create an S3 bucket with versioning and encryption enabled" and CodeWhiper creates the code to enable versioning and encryption.

## Outcome
![CLI Upload Command](942e19c2c0626cdcaeb9a50da164960.png "CLI Upload Command")

![S3 Bucket Interface](178ed85dca92908751795ba63b16958.png "S3 Bucket Interface")
The json file miniproj3 has been added to the bucket



